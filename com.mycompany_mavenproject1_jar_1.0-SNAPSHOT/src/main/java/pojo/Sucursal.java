/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Sistemas26
 */
public class Sucursal {
    private int id;
    private Cliente cliente;
    private String nombreSucursal;
    private String responsable;
    private String direccion;
    private String telefono;

    public Sucursal() {
    }

    public Sucursal(int id, Cliente cliente, String nombreSucursal, String responsable, String direccion, String telefono) {
        this.id = id;
        this.cliente = cliente;
        this.nombreSucursal = nombreSucursal;
        this.responsable = responsable;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", cliente=" + cliente + ", nombreSucursal=" + nombreSucursal + ", responsable=" + responsable + ", direccion=" + direccion + ", telefono=" + telefono + '}';
    }
    
    
    
}
