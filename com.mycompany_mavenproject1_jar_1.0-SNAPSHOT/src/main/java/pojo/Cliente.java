/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.Date;

/**
 *
 * @author Sistemas26
 */
public class Cliente {
    private int id;
    private String nombres;
    private String apellidos;
    private String nombreEmpresa;
    private String telefono;
    private String cedula;
    private String correo;
    private String direccion;
    private int activo;
    private Date fechaCreacion;

    public Cliente() {
    }

    public Cliente(int id, String nombres, String apellidos, String nombreEmpresa, String telefono, String cedula, String correo, String direccion, int activo, Date fechaCreacion) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nombreEmpresa = nombreEmpresa;
        this.telefono = telefono;
        this.cedula = cedula;
        this.correo = correo;
        this.direccion = direccion;
        this.activo = activo; //1-Activo 0-Inactivo
        this.fechaCreacion = fechaCreacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", nombreEmpresa=" + nombreEmpresa + ", telefono=" + telefono + ", cedula=" + cedula + ", correo=" + correo + ", direccion=" + direccion + ", activo=" + activo + ", fechaCreacion=" + fechaCreacion + '}';
    }
    
    
}
