/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Sistemas26
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int op = 0;
        Scanner entrada = new Scanner(System.in);
        int opSeguir = 0;
        boolean seguir = true;
        int opMenu = 0;
        List<Cliente> clientes = new ArrayList<>();
        List<Sucursal> sucursales = new ArrayList<>();
        while (seguir) {
            do {
                System.out.println("1. Registrar cliente y sucursal");
                System.out.println("2. Listar cliente por cedula");
                System.out.println("3. Listar las sucursales por cliente");
                System.out.println("4. Listar clientes activos");
                System.out.println("5. Listar todas las sucursales");
                System.out.println("6. Listar clientes por rango de fecha");
                opMenu = entrada.nextInt();
            } while (opMenu < 0 || opMenu > 6);

            switch (opMenu) {
                case 1:
                    int idS = sucursales.size();
                    System.out.println("cliente: ");
                    int id = clientes.size();
                    System.out.println("Nombres: ");
                    entrada.nextLine();
                    String nombres = entrada.nextLine();
                    System.out.println("Apellidos: ");
                    String apellidos = entrada.nextLine();
                    System.out.println("Nombre de la empresa: ");
                    String nomEmp = entrada.nextLine();
                    System.out.println("Telefono: ");
                    String telefono = entrada.nextLine();
                    System.out.println("Cedula: ");
                    String cedula = entrada.nextLine();
                    System.out.println("Correo: ");
                    String correo = entrada.nextLine();
                    System.out.println("Direccion: ");
                    String di = entrada.nextLine();
                    System.out.println("¿Es activo?\n1.SI  0.NO");
                    int activo = entrada.nextInt();
                    Date d = new Date();
                    Cliente nuevoCliente = new Cliente(id, nombres, apellidos, nomEmp, telefono, cedula, correo, di, activo, d);
                    clientes.add(nuevoCliente);
                    System.out.println("nombre de la Sucursal: ");
                    String nomS = entrada.nextLine();
                    System.out.println("Responsable: ");
                    String res = entrada.nextLine();
                    System.out.println("Direccion: ");
                    String dir = entrada.nextLine();
                    System.out.println("Telefono: ");
                    String tel = entrada.nextLine();
                    Sucursal nuevaSucur = new Sucursal(idS, nuevoCliente, nomS, res, dir, tel);
                    sucursales.add(nuevaSucur);
                  

                    int opSeguirRegistrando = 1;
                    while (opSeguirRegistrando == 1) {
                        do {
                            System.out.println("¿Desea registar otro cliente?");
                            opSeguirRegistrando = entrada.nextInt();
                        } while (opSeguirRegistrando < 0 || opSeguirRegistrando > 1);
                    }
                    break;
                case 2:
                    System.out.println("Ingrese el numero de cedula a buscar: ");
                    String cedulaBuscar = entrada.next();

                    for (Cliente cliente : clientes) {
                        if (cliente.getCedula().compareTolgnoringCase(cedulaBuscar) == 0) {
                            System.out.println(cliente.toString());
                        }
                    }
                    break;
                case 3:
                    

                    break;
                case 4:
                    for (Cliente cliente : clientes) {
                        if (cliente.getActivo() == 1) {
                            System.out.println(cliente.toString());
                        }
                    }
                    break;
                case 5:
                    for (Sucursal s : sucursales) {
                        System.out.println(s.toString());
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Opcion no valida!");
                    throw new AssertionError();
            }

            do {
                System.out.println("Desea continuar en el sistema?\n 1.SI 0.NO");
                opSeguir = entrada.nextInt();
            } while (opSeguir < 0 || opSeguir > 1);
        }
    }

}
