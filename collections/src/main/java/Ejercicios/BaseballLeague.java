/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author Sistemas39
 */
public class BaseballLeague {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner (System.in);
       Queue<String> nombres = new LinkedList<String>();
       int option =0;
       do{
           System.out.println("Ingrese una opcion");
           menu();
           option = entrada.nextInt();
           switch(option){
               case 1:
                   System.out.println("Ingrese el nombre de el empleado");
                   String nombre = entrada.next();
                   nombres.offer(nombre);
                   break;
               case 2:
                   System.out.println(Arrays.toString(nombres.toArray()));
                   break;
               case 3: 
                   System.out.println(nombres.poll());
                   break;
               case 4:
                   break;
               default:
                   throw new AssertionError();
           }
       }while (option !=4);
       
    }
    
     public static void menu(){
         System.out.println("1. Ingresar empleado de la cola");
         System.out.println("2. Mostrar cola");
         System.out.println("3. Sacar empleado de la cola");
     }
}
