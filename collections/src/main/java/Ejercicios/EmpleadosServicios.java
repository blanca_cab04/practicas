/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicios;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Sistemas39
 */
public class EmpleadosServicios {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Set <String> set = new HashSet<>();
       set.add("Rodrigo");
       set.add("Blanca");
       set.add("Jeffrey");
       set.add("Gladys");
       TreeSet sortedSet = new TreeSet<>(set);
        System.out.println("El primer elemento sacado de la lista es: "+(String)sortedSet.first());
    }
    
}
