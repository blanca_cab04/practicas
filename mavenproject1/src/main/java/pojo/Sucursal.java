/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Sistemas36
 */
public class Sucursal {
    private int id;
    private String nombreSucursal;
    private String representante;
    private String municipio;
    private String direccion;
    private String telefono;
    private String correo;
    private boolean activo;
    private String fechaCreac;
    private String fechaMod;

    public Sucursal() {
    }

    public Sucursal(int id, String nombreSucursal, String representante, String municipio, String direccion, String telefono, String correo, boolean activo, String fechaCreac, String fechaMod) {
        this.id = id;
        this.nombreSucursal = nombreSucursal;
        this.representante = representante;
        this.municipio = municipio;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.activo = activo;
        this.fechaCreac = fechaCreac;
        this.fechaMod = fechaMod;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getFechaCreac() {
        return fechaCreac;
    }

    public void setFechaCreac(String fechaCreac) {
        this.fechaCreac = fechaCreac;
    }

    public String getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(String fechaMod) {
        this.fechaMod = fechaMod;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", nombreSucursal=" + nombreSucursal + ", representante=" + representante + ", municipio=" + municipio + ", direccion=" + direccion + ", telefono=" + telefono + ", correo=" + correo + ", activo=" + activo + ", fechaCreac=" + fechaCreac + ", fechaMod=" + fechaMod + '}';
    }
    
    
}
